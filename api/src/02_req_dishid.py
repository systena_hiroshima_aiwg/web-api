from flask import Flask, jsonify, request
app = Flask(__name__)

@app.route('/reqDishId/', methods=['POST'])
def post_request():
	result = {
		"status": 1,
		"areas": [
		{
			"areaid": request.form["areaid1"],
			"dishid": 1
		},{
			"areaid": request.form["areaid2"],
			"dishid": 2
		}]
	}

	return jsonify(ResultSet=result)

if __name__ == '__main__':
	app.run(host='0.0.0.0')
