from flask import Flask, jsonify, request
import json

app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def test_request():
	result = {
		"a": request.json 
	}

	return jsonify(result)

if __name__ == '__main__':
	app.run(host='0.0.0.0')
